from django.urls import path, include

urlpatterns = [
    path("", include("fluent_blogs.urls")),
    path("/comments/", include("django_comments.urls")),
]
